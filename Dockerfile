FROM node:18-slim

WORKDIR /tools
COPY package.json yarn.lock .

RUN yarn install && yarn cache clean

ENV PATH="/tools/node_modules/.bin:${PATH}"

WORKDIR /app

ENTRYPOINT ["/bin/bash"]
